# Add a new app
Digilab offers multi-tenant clusters, meaning that one or more applications developed/maintained by one or more teams may run on the same cluster.

A team can host their own Flux repository containing the manifests of their application(s). You just need to make sure the cluster has connectivity to that repository, e.g. via the internet. Digilab can provide the public SSH key you need to add as read-only deploy key to your Git repository.

It is very easy to add a new application when your team is already onboarded to Digilab hosting. There is a [GitLab pipeline](https://gitlab.com/digilab.overheid.nl/platform/flux/core/-/pipelines/new) available that generates the required configuration for adding your Flux repository to the cluster. The pipeline will create a Merge Request (MR) that will be reviewed by the Digilab team. When approved (and merged), the application defined in your Flux repository will be deployed to the cluster.
