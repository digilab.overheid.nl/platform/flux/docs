# Add a new team

Digilab offers multi-tenant clusters, meaning that several teams may run their applications on the same cluster.

For onboarding a new team to Digilab hosting, there is a [GitLab pipeline](https://gitlab.com/digilab.overheid.nl/platform/flux/create-team/-/pipelines/new) available that generates the required configuration.
The pipeline will create a Merge Request (MR) that will be reviewed by the Digilab team.
When approved (and merged), [new apps can be added](add-a-new-app.md) for the team.
