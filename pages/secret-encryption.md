# Secret encryption
To achieve full GitOps, all resources should be in Git. An application might need secrets, such as API keys to communicate with external services. It is a bad practice though, to store secrets in Git. Therefore, secrets are asymmetrically encrypted using [SOPS](https://github.com/getsops/sops) + [age](https://github.com/FiloSottile/age).

sops is an editor of encrypted files that supports YAML, JSON, ENV, INI and BINARY formats and encrypts with AWS KMS, GCP KMS, Azure Key Vault, age, and PGP.

age is a simple, modern and secure file encryption tool, format, and Go library.

age is natively supported by SOPS.

When installing Flux, a keypair is generated and directly stored in the cluster. This is the only place where it's stored, so we store a copy in Azure Keyvault, allowing is to recreate a cluster from scratch with state. Secrets are encrypted locally with the public key, stored in Git, and decrypted by Flux on the fly.

When creating a new tenant, the keypair is stored in Git, but encrypted with the public key from the previous paragraph. This allows Flux to decrypt it and store it in the cluster. When a tenant wants to add a secret, that tenant can use their own public key to encrypt the secret and store it in Git. Flux will automatically select the correct private key, decrypt the secret and store it in the cluster, available for use by the tenant. A dedicated Kubernetes service account for the tenant is used to apply the resources. This service account only has access to its own namespace.

Public keys are stored in [this repository](https://gitlab.com/digilab.overheid.nl/platform/flux/core/-/tree/main/sops-public-keys?ref_type=heads). To encrypt a Kubernetes secret using such a public key, use the following command:

```bash
sops \
  --encrypt \
  --encrypted-regex '^(data|stringData)$' \
  --age "[age-pub-key]" \
  --in-place \
  my-kubernetes-secret.yaml
```

The `encrypted-regex` only encrypts the secret values, not other metadata, which keeps the Kubernetes secret readable and identifiable. You can safely commit the encrypted secret. When Flux reconciles the updated manifests, it will be able to automatically decrypt the secret and apply it.
