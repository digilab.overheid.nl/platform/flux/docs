# Grafana

Digilab offers dedicated Grafana instances per team.

The provision the instance, run the [GitLab pipeline](https://gitlab.com/digilab.overheid.nl/platform/flux/create-team/-/pipelines/new) and set the variable `has_grafana` to `true`.
This pipeline will create 
* an MR on [applications](https://gitlab.com/digilab.overheid.nl/platform/terraform/applications) to create an Authentik application for Grafana for the team.
* an MR on [core](https://gitlab.com/digilab.overheid.nl/platform/flux/core) to create the manifests.
