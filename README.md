# Digilab hosting
- [Add a new team](pages/add-a-new-team.md)
- [Add a new app](pages/add-a-new-app.md)
- [Grafana](pages/grafana.md)
- [Secret encryption](pages/secret-encryption.md)
